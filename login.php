<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		   <link rel="stylesheet" href="css/base.css"/>
		<title>Log in</title>
	</head>
	<body>
		<?php
		if (!empty($_REQUEST['success'])){
			echo "<p class='success'>" . $_REQUEST['success'] . "</p>";
		}
		if (!empty($_REQUEST['error'])){
			echo "<p class='error'>" . $_REQUEST['error'] . "</p>";
		}	
		?>
	<h1>Log in</h1>
		<form action="processLogin.php" method="POST">
			<input type="text" name="username" placeholder="Username"> <br>
			<input type="password" name="password" placeholder="Password"> <br><br>
			<input type ="submit" value="Log in">
		</form>
		<br><a href = "register.php">Register</a>
	</body>
</html>