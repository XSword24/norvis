<?php
require_once ('connect.php');

	$username = htmlspecialchars($_POST["username"]);
	$password = htmlspecialchars($_POST["password"]);

	if(empty($username) or empty($password)){
		$error = "Empty username or password!";
		header("Location: login.php?error=$error") or die("Error when redirecting to the register page.");
	}
	
	$stmt = $pdo->query("SELECT username, password FROM accounts WHERE username = '$username';");
	$data = $stmt ->fetch();
	
	if (is_null($data["username"])){
		$error = "Username doesn't exist!";
		header("Location: login.php?error=$error") or die("Error when redirecting to the register page.");
	}
	//Check password
	$stmt = $pdo->query("SELECT password FROM accounts WHERE username = '$username';");
	$data = $stmt ->fetch();
	
	if (password_verify($password, $data["password"])){
		$success ="Hi $username!";
		header("Location: login.php?success=$success") or die("Error when redirecting to the register page.");
	} else{
		$error = "Wrong password!";
		header("Location: login.php?error=$error") or die("Error when redirecting to the register page.");
	}
	
?>