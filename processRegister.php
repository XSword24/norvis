<?php
require_once('connect.php');
	
	$success ="";
	$error = "";
	$username = htmlspecialchars($_POST["username"]);
	$email = htmlspecialchars($_POST["email"]);
	$password = htmlspecialchars($_POST["password"]);
	
	if(empty($username) or empty($email) or empty($password) or !filter_var($email,
		FILTER_VALIDATE_EMAIL)){
		$error = "One or more input values are invalid!";
		header("Location: register.php?error=$error") or die("Error when redirecting to the register page.");
	} else{
		//query
		$stmt = $pdo->query("SELECT username FROM accounts WHERE username = '$username';");
		$data = $stmt ->fetch();
		
		if(!is_null($data["username"])){
			$error="Username already exists!";
			header("Location: register.php?error=$error") or die("Error when redirecting to the register page.");
		}
		
		$stmt = $pdo->query("SELECT email FROM accounts WHERE email = '$email';");
		$data = $stmt ->fetch();
		
		if(!is_null($data["email"])){
			$error="Email already in use!";
			header("Location: register.php?error=$error") or die("Error when redirecting to the register page.");
		}
		//Insert
		$password = password_hash($password, PASSWORD_DEFAULT);
		$sql = "INSERT INTO accounts (username, email, password) VALUES (?,?,?)";
		$aux = $pdo->prepare($sql);
		$check = $aux->execute([$username, $email, $password]);
	}
	
	if($check){
		$success = "Registration sucessful!";
		header("Location: register.php?success=$success") or die("Error when redirecting to the register page.");
	} else{
		$error = "Error when adding the data to the database.";
		header("Location: register.php?error=$error") or die("Error when redirecting to the register page.");
	}
?>